package com.example.train.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.constants.Constants;
import com.example.train.entity.Booking;
import com.example.train.entity.Payment;
import com.example.train.entity.User;
import com.example.train.exception.BookinIdException;
import com.example.train.repository.BookingRepository;
import com.example.train.repository.PaymentRepository;
import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;

@SpringBootTest
public class PaymentserviceTest {
	@Mock
	PaymentRepository paymentRepository;
	@Mock
	BookingRepository bookingRepository;

	@InjectMocks
	Paymentservice paymentService;
	Booking booking;

	@BeforeEach
	public void init() {
		User u = new User();
		u.setUserId(1l);
		booking = new Booking();
		booking.setBookingDate(LocalDate.now());
		booking.setBookingId(1L);
		booking.setBookingNumber(123l);
		booking.setBookingStatus(false);
		booking.setPaymentStatus("pending");
		booking.setNoOfSeats(3);
		booking.setPassengers(null);
		booking.setTotalAmount(12000d);
		booking.setTrainName("kanti");
		booking.setUser(u);
		List<Booking> booksList = new ArrayList<Booking>();
		booksList.add(booking);

	}

	@Test
	public void process() {

		PaymentRequest payment = new PaymentRequest();
		payment.setAmount(12000d);
		payment.setBookingNumber(123l);
		payment.setPaymentType("paytm");
		Payment pay = new Payment();

		Mockito.when(paymentRepository.save(pay)).thenReturn(pay);
		Mockito.when(bookingRepository.findByBookingNumber(payment.getBookingNumber()))
				.thenReturn(Optional.of(booking));
		ResponseDto process = paymentService.processPayment(payment);
		Assertions.assertEquals(Constants.PAYMENT_SUCCESS, process.getMessage());

	}

	@Test
	public void paymenDone() {
		User u = new User();
		u.setUserId(1l);
		Booking booking = new Booking();
		booking.setBookingDate(LocalDate.now());
		booking.setBookingId(1L);
		booking.setBookingNumber(123l);
		booking.setBookingStatus(true);
		booking.setPaymentStatus("pending");
		booking.setNoOfSeats(3);
		booking.setPassengers(null);
		booking.setTotalAmount(12000d);
		booking.setTrainName("kanti");
		booking.setUser(u);
		List<Booking> booksList = new ArrayList<Booking>();
		booksList.add(booking);

		PaymentRequest payment = new PaymentRequest();
		payment.setAmount(12000d);
		payment.setBookingNumber(123l);
		payment.setPaymentType("paytm");
		Payment pay = new Payment();
		try {
			Mockito.when(paymentRepository.save(pay)).thenReturn(pay);
			Mockito.when(bookingRepository.findByBookingNumber(payment.getBookingNumber()))
					.thenReturn(Optional.of(booking));
			ResponseDto process = paymentService.processPayment(payment);
		} catch (BookinIdException details) {
			Assertions.assertEquals(Constants.PAYMENT_HAS_MADE_EARLIER, details.getMessage());
		}
	}

//	@Test
//	public void paymentIDNotFound() {
////		User u = new User();
////		u.setUserId(1l);
////		Booking booking = new Booking();
////		booking.setBookingDate(LocalDate.now());
////		booking.setBookingId(1L);
////		booking.setBookingNumber(1l);
////		booking.setBookingStatus(false);
////		booking.setPaymentStatus("pending");
////		booking.setNoOfSeats(3);
////		booking.setPassengers(null);
////		booking.setTotalAmount(12000d);
////		booking.setTrainName("kanti");
////		booking.setUser(u);
//		List<Booking> booksList = new ArrayList<Booking>();
////		booksList.add(booking);
//
//		PaymentRequest payment = new PaymentRequest();
//		payment.setAmount(12000d);
//		payment.setBookingNumber(12367889l);
//		payment.setPaymentType("paytm");
//		Payment pay = new Payment();
//		try {
//			Mockito.when(paymentRepository.save(pay)).thenReturn(pay);
//			Mockito.when(bookingRepository.findByBookingNumber(payment.getBookingNumber()))
//					.thenReturn(Optional.of(booking));
//			ResponseDto process = paymentService.processPayment(payment);
//		} catch (BookinIdException details) {
//			Assertions.assertEquals(Constants.BOOKING_ID_NOT_FOUND, details.getMessage());
//		}
//	}

}

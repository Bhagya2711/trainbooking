
/**
 * 
 */
package com.example.train.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.train.response.TrainDetailResponse;
import com.example.train.response.TrainResponse;
import com.example.train.service.TrainServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class TrainControllerTest {

	@InjectMocks
	TrainController trainController;

	@Mock
	TrainServiceImpl trainServiceImpl;

	TrainDetailResponse trainDetailResponse = new TrainDetailResponse();

	@BeforeEach
	public void init() {

	}

	@Test
	public void testSearch() {
		TrainResponse trainResponse = new TrainResponse(1L, "express", 123L, "hyd", "blr", LocalTime.now(),
				LocalTime.now().plusHours(10));
		List<TrainResponse> trainResponses = new ArrayList<TrainResponse>();
		trainResponses.add(trainResponse);
		Mockito.when(trainServiceImpl.search("hyd", "blgr", LocalDate.now())).thenReturn(trainResponses);

		ResponseEntity<Object> search = trainController.search("hyd", "blr", LocalDate.now());

		Assertions.assertEquals(HttpStatus.OK, search.getStatusCode());

	}

	@Test
	public void searchTrainById() {
		Mockito.when(trainServiceImpl.searchById(1L)).thenReturn(trainDetailResponse);
		ResponseEntity<TrainDetailResponse> searchById = trainController.searchById(1L);
		Assertions.assertEquals(HttpStatus.OK, searchById.getStatusCode());
	}

}

/**
 * 
 */
package com.example.train.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.exception.UserNotFound;
import com.example.train.request.BookingRequest;
import com.example.train.request.PassengerDto;
import com.example.train.response.BookingHistory;
import com.example.train.response.BookingResponse;
import com.example.train.service.UserServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserServiceImpl userServiceImpl;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testGetBookingHistory() {

		BookingHistory bookingHistory = new BookingHistory(1L, 11223l, LocalDate.now(), true, 1000.0, "falaknaama",
				10);

		List<BookingHistory> historyList = new ArrayList<>();
		historyList.add(bookingHistory);

		Mockito.when(userServiceImpl.getBookingHistory(1L)).thenReturn(historyList);
		assertEquals(200, userController.getBookingHistory(1L).getStatusCodeValue());

	}

	@Test
	public void testBookTicket() throws UserNotFound{

		PassengerDto passenger=new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");
		
		List<PassengerDto> passengers=new ArrayList<>();
		passengers.add(passenger);
		
		BookingRequest bookingRequest=new BookingRequest(1L, 1L, 1L, passengers);
		
		BookingResponse bookingResponse=new BookingResponse("booked", 112337l, 1000.0, 200l);
		Mockito.when(userServiceImpl.bookTicket(bookingRequest)).thenReturn(bookingResponse);
		assertEquals(200, userController.bookTicket(bookingRequest).getStatusCodeValue());

	}

}

/**
 * 
 */
package com.example.train.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;
import com.example.train.service.AdapatorService;
import com.example.train.service.GooglePay;
import com.example.train.service.ServiceRegistry;

/**
 * @author hemanth.garlapati
 * 
 */
@ExtendWith(MockitoExtension.class)
public class PaymentControllerTest {
	
	@InjectMocks
	PaymentController paymentController;

	@Mock
	ServiceRegistry serviceRegistry;
	
	
	GooglePay googlePay;

	

	@Test
	public void testprocessPayment() {
		AdapatorService<Object> adapatorService=new AdapatorService<Object>() {
			
			@Override
			public ResponseDto process(Object request) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		PaymentRequest paymentRequest=new PaymentRequest();
		paymentRequest.setAmount(1000.0);
		paymentRequest.setBookingNumber(524378L);
		paymentRequest.setPaymentType("googlepay");
		
		Mockito.when(serviceRegistry.getService("googlepay")).thenReturn(adapatorService);
		assertEquals(200, paymentController.processPayment(paymentRequest).getStatusCodeValue());

	}

}

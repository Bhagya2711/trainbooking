/**
 * 
 */
package com.example.train.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.exception.UserNotFound;
import com.example.train.request.LoginDto;
import com.example.train.response.ResponseDto;
import com.example.train.service.LoginServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class LoginControllerTest {
	

	@Mock
	LoginServiceImpl loginServiceImpl;

	@InjectMocks
	LoginController loginController;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testUserLogin() throws UserNotFound  {
		LoginDto loginDto=new LoginDto("abc123", "abc123");
		ResponseDto responseDto=new ResponseDto("logged in sucessfully", 200l);
		Mockito.when(loginServiceImpl.userLogin(loginDto)).thenReturn(responseDto);
		assertEquals(200, loginController.userLogin(loginDto).getStatusCodeValue());
	}
	

}

/**
 * 
 */
package com.example.train.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.entity.User;
import com.example.train.exception.UserNotFound;
import com.example.train.repository.UserRepository;
import com.example.train.request.LoginDto;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	UserRepository userRepository;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testLogin() throws UserNotFound {

		User user = new User(1L, "abc", "abc@gmail.com", "ABC123", null);

		LoginDto loginDto = new LoginDto("abc@gmail.com", "ABC123");
		Mockito.when(userRepository.findByUserEmailAndUserPassword("abc@gmail.com", "ABC123"))
				.thenReturn(Optional.of(user));

		assertEquals(200, loginServiceImpl.userLogin(loginDto).getCode());

	}

	@Test
	public void testLoginCustomerNotFound() {

		User user = new User(1L, "abc", "abc@gmail.com", "ABC123", null);
		LoginDto loginDto = new LoginDto("abc@gmail.com", "ABC123");
		Mockito.when(userRepository.findByUserEmailAndUserPassword("abc@gmail.com", "ABC1234"))
				.thenReturn(Optional.of(user));
		assertThrows(UserNotFound.class, () -> loginServiceImpl.userLogin(loginDto));

	}

}

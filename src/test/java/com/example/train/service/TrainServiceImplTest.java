package com.example.train.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.entity.Train;
import com.example.train.entity.TrainClass;
import com.example.train.exception.TrainNotFoundExceptionHandler;
import com.example.train.repository.TrainRepository;
import com.example.train.response.TrainDetailResponse;
import com.example.train.response.TrainResponse;

@SpringBootTest
public class TrainServiceImplTest {
	@Mock
	TrainRepository trainRepository;
	@InjectMocks
	TrainServiceImpl trainServiceImpl;
	Train train = new Train();
	ArrayList<Train> trainlist = new ArrayList<>();

	@BeforeEach
	void init() {
		TrainClass trainclass = new TrainClass();
		ArrayList<TrainClass> list = new ArrayList<TrainClass>();

		train.setArrivalTime(LocalTime.now());
		train.setDepartureTime(LocalTime.now());
		train.setDestination("mumbai");
		train.setSource("ban");
		train.setTrainId(1L);
		train.setTrainName("kanceco express");
		train.setTrainNumber(11100L);
		train.setTrainRunningDate(LocalDate.now());
		trainclass.setAvailableSeats(12);
		trainclass.setClassName("2c");
		trainclass.setFare(200d);
		trainclass.setTrain(train);
		trainclass.setTrainClassId(1L);
		list.add(trainclass);
		train.setTrainClass(list);
		trainlist.add(train);
	}

	@Test
	public void Test_searchById() {

		Mockito.when(trainRepository.findById(1L)).thenReturn(Optional.of(train));
		TrainDetailResponse searchById = trainServiceImpl.searchById(1l);
		Assertions.assertEquals("kanceco express", searchById.getTrainName());

	}

	@Test
	public void test_searchByIdNegative() {
		try {
			Mockito.when(trainRepository.findById(3L)).thenReturn(Optional.of(train));
			TrainDetailResponse searchById = trainServiceImpl.searchById(3l);
		} catch (TrainNotFoundExceptionHandler exception) {
			Assertions.assertEquals("train not found", exception.getLocalizedMessage());
		}
	}

	@Test
	public void testSearchTrains() {

		Mockito.when(
				trainRepository.findBySourceIgnoreCaseContainingAndDestinationIgnoreCaseContainingAndTrainRunningDate(
						"ban", "mumbai", LocalDate.now()))
				.thenReturn(trainlist);

		List<TrainResponse> trains = trainServiceImpl.search("ban", "mumbai", LocalDate.now());
		Assertions.assertNotNull(trains);

	}
}

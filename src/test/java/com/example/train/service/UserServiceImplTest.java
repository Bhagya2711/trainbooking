/**
 * 
 */
package com.example.train.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.entity.Booking;
import com.example.train.entity.Passenger;
import com.example.train.entity.Train;
import com.example.train.entity.TrainClass;
import com.example.train.entity.User;
import com.example.train.exception.NoSeatAvailable;
import com.example.train.exception.PassengerLimitExceededException;
import com.example.train.exception.TrainClassNotFound;
import com.example.train.exception.TrainNotFoundExceptionHandler;
import com.example.train.exception.UserNotFound;
import com.example.train.repository.BookingRepository;
import com.example.train.repository.PassengerRepository;
import com.example.train.repository.TrainClassRepository;
import com.example.train.repository.TrainRepository;
import com.example.train.repository.UserRepository;
import com.example.train.request.BookingRequest;
import com.example.train.request.PassengerDto;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	UserRepository userRepository;

	@Mock
	TrainRepository trainRepository;

	@Mock
	TrainClassRepository trainClassRepository;
	
	@Mock
	BookingRepository bookingRepository;
	
	@Mock
	PassengerRepository passengerRepository;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testGetBookingHistory() {

		Passenger passenger = new Passenger();
		passenger.setPassengerId(1L);
		passenger.setPassengerName("xyz");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("134567");

		List<Passenger> passengers = new ArrayList<>();
		passengers.add(passenger);

		Booking booking = new Booking();
		booking.setBookingId(1L);
		booking.setBookingNumber(1233L);
		booking.setBookingDate(LocalDate.now());
		booking.setBookingStatus(false);
		booking.setNoOfSeats(1);
		booking.setTotalAmount(1000.0);
		booking.setTrainName("kachiguda");
		booking.setPassengers(passengers);

		List<Booking> bookings = new ArrayList<>();
		bookings.add(booking);

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");
		user.setBooking(bookings);

		when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		assertEquals(1, userServiceImpl.getBookingHistory(1L).size());

	}

	@Test
	public void testBookTicket() throws UserNotFound {

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");

		PassengerDto passenger = new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");

		List<PassengerDto> passengers = new ArrayList<>();
		passengers.add(passenger);

		BookingRequest bookingRequest = new BookingRequest(1L, 1L, 1L, passengers);

		Train train = new Train();
		train.setArrivalTime(LocalTime.now().plusHours(10));
		train.setDepartureTime(LocalTime.now());
		train.setDestination("hyd");
		train.setSource("blr");
		train.setTrainId(1L);
		train.setTrainName("blg-hyd");
		train.setTrainRunningDate(LocalDate.now());

		TrainClass trainClass = new TrainClass(1L, "3T", 20, train, 100.0);

		when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		when(trainRepository.findById(1l)).thenReturn(Optional.of(train));
		when(trainClassRepository.findById(1l)).thenReturn(Optional.of(trainClass));
		assertEquals(100.0, userServiceImpl.bookTicket(bookingRequest).getAmount());

	}
	
	@Test
	public void testBookTicketForUserNotFound() throws UserNotFound {

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");

		PassengerDto passenger = new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");

		List<PassengerDto> passengers = new ArrayList<>();
		passengers.add(passenger);

		BookingRequest bookingRequest = new BookingRequest(1L, 1L, 1L, passengers);

		Train train = new Train();
		train.setArrivalTime(LocalTime.now().plusHours(10));
		train.setDepartureTime(LocalTime.now());
		train.setDestination("hyd");
		train.setSource("blr");
		train.setTrainId(1L);
		train.setTrainName("blg-hyd");
		train.setTrainRunningDate(LocalDate.now());

		TrainClass trainClass = new TrainClass(1L, "3T", 20, train, 100.0);

		//when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		when(trainRepository.findById(1l)).thenReturn(Optional.of(train));
		when(trainClassRepository.findById(1l)).thenReturn(Optional.of(trainClass));
		assertThrows(UserNotFound.class, () -> userServiceImpl.bookTicket(bookingRequest));
	}
	
	@Test
	public void testBookTicketForTrainNotFound() throws UserNotFound {

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");

		PassengerDto passenger = new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");

		List<PassengerDto> passengers = new ArrayList<>();
		passengers.add(passenger);

		BookingRequest bookingRequest = new BookingRequest(1L, 1L, 1L, passengers);

		Train train = new Train();
		train.setArrivalTime(LocalTime.now().plusHours(10));
		train.setDepartureTime(LocalTime.now());
		train.setDestination("hyd");
		train.setSource("blr");
		train.setTrainId(1L);
		train.setTrainName("blg-hyd");
		train.setTrainRunningDate(LocalDate.now());

		TrainClass trainClass = new TrainClass(1L, "3T", 20, train, 100.0);

		when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		//when(trainRepository.findById(1l)).thenReturn(Optional.of(train));
		when(trainClassRepository.findById(1l)).thenReturn(Optional.of(trainClass));
		assertThrows(TrainNotFoundExceptionHandler.class, () -> userServiceImpl.bookTicket(bookingRequest));
	}
	
	@Test
	public void testBookTicketForTrainClassNotFound() throws UserNotFound {

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");

		PassengerDto passenger = new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");

		List<PassengerDto> passengers = new ArrayList<>();
		passengers.add(passenger);

		BookingRequest bookingRequest = new BookingRequest(1L, 1L, 1L, passengers);

		Train train = new Train();
		train.setArrivalTime(LocalTime.now().plusHours(10));
		train.setDepartureTime(LocalTime.now());
		train.setDestination("hyd");
		train.setSource("blr");
		train.setTrainId(1L);
		train.setTrainName("blg-hyd");
		train.setTrainRunningDate(LocalDate.now());


		when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		when(trainRepository.findById(1l)).thenReturn(Optional.of(train));
		//when(trainClassRepository.findById(1l)).thenReturn(Optional.of(trainClass));
		assertThrows(TrainClassNotFound.class, () -> userServiceImpl.bookTicket(bookingRequest));
	}
	
	@Test
	public void testBookTicketForNoseatsAvailable() throws UserNotFound {

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");

		PassengerDto passenger = new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");

		List<PassengerDto> passengers = new ArrayList<>();
		passengers.add(passenger);

		BookingRequest bookingRequest = new BookingRequest(1L, 1L, 1L, passengers);

		Train train = new Train();
		train.setArrivalTime(LocalTime.now().plusHours(10));
		train.setDepartureTime(LocalTime.now());
		train.setDestination("hyd");
		train.setSource("blr");
		train.setTrainId(1L);
		train.setTrainName("blg-hyd");
		train.setTrainRunningDate(LocalDate.now());

		TrainClass trainClass = new TrainClass(1L, "3T", 0, train, 100.0);

		when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		when(trainRepository.findById(1l)).thenReturn(Optional.of(train));
		when(trainClassRepository.findById(1l)).thenReturn(Optional.of(trainClass));
		assertThrows(NoSeatAvailable.class, () -> userServiceImpl.bookTicket(bookingRequest));
	}
	
	@Test
	public void testBookTicketForMaxPassengers() throws UserNotFound {

		User user = new User();
		user.setUserId(1l);
		user.setUserName("abc");
		user.setUserPassword("abc");
		user.setUserEmail("abc@gmail.com");

		PassengerDto passenger = new PassengerDto();
		passenger.setPassengerName("qwerty");
		passenger.setPassengerAge(22);
		passenger.setAadharNumber("123445");

		List<PassengerDto> passengers = new ArrayList<>();
		passengers.add(passenger);
		passengers.add(passenger);
		passengers.add(passenger);
		passengers.add(passenger);
		passengers.add(passenger);
		passengers.add(passenger);

		BookingRequest bookingRequest = new BookingRequest(1L, 1L, 1L, passengers);

		Train train = new Train();
		train.setArrivalTime(LocalTime.now().plusHours(10));
		train.setDepartureTime(LocalTime.now());
		train.setDestination("hyd");
		train.setSource("blr");
		train.setTrainId(1L);
		train.setTrainName("blg-hyd");
		train.setTrainRunningDate(LocalDate.now());

		TrainClass trainClass = new TrainClass(1L, "3T", 0, train, 100.0);

		when(userRepository.findById(1l)).thenReturn(Optional.of(user));
		when(trainRepository.findById(1l)).thenReturn(Optional.of(train));
		when(trainClassRepository.findById(1l)).thenReturn(Optional.of(trainClass));
		assertThrows(PassengerLimitExceededException.class, () -> userServiceImpl.bookTicket(bookingRequest));
	}

}

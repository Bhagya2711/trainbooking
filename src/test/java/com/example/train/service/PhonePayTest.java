package com.example.train.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.train.entity.Booking;
import com.example.train.entity.Payment;
import com.example.train.entity.User;
import com.example.train.exception.BookinIdException;
import com.example.train.repository.BookingRepository;
import com.example.train.repository.PaymentRepository;
import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;
import com.example.train.utils.Paymentservice;

/**
 * @author rakeshr.ra
 *
 */
@SpringBootTest
public class PhonePayTest {
	@Mock
	Paymentservice paymentservice;

	@InjectMocks
	PhonePay phonepay;

	@BeforeEach
	public void init() {

	}

	@Test
	public void process() {
		PaymentRequest payment = new PaymentRequest();
		payment.setAmount(12000d);
		payment.setBookingNumber(123l);
		payment.setPaymentType("googlepay");
		ResponseDto res = new ResponseDto();
		res.setMessage("payment success");

		Mockito.when(paymentservice.processPayment(payment)).thenReturn(res);
		ResponseDto process = phonepay.process(payment);
		Assertions.assertNotNull(process);
	}

}
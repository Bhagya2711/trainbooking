/**
 * 
 */
package com.example.train.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.train.constants.Constants;
import com.example.train.entity.Booking;
import com.example.train.entity.Passenger;
import com.example.train.entity.Train;
import com.example.train.entity.TrainClass;
import com.example.train.entity.User;
import com.example.train.exception.NoSeatAvailable;
import com.example.train.exception.PassengerLimitExceededException;
import com.example.train.exception.TrainClassNotFound;
import com.example.train.exception.TrainNotFoundExceptionHandler;
import com.example.train.exception.UserNotFound;
import com.example.train.repository.BookingRepository;
import com.example.train.repository.PassengerRepository;
import com.example.train.repository.TrainClassRepository;
import com.example.train.repository.TrainRepository;
import com.example.train.repository.UserRepository;
import com.example.train.request.BookingRequest;
import com.example.train.request.PassengerDto;
import com.example.train.response.BookingHistory;
import com.example.train.response.BookingResponse;

/**
 * @author hemanth.garlapati
 * 
 */
@Service
public class UserServiceImpl implements UserService {


	@Autowired
	BookingRepository bookingRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PassengerRepository passengerRepository;

	@Autowired
	TrainClassRepository trainClassRepository;

	@Autowired
	TrainRepository trainRepository;
	
	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	public BookingResponse bookTicket(BookingRequest bookingRequest) throws UserNotFound {

		Optional<User> optionalUser = userRepository.findById(bookingRequest.getUserId());

		Booking booking = new Booking();

		if (!optionalUser.isPresent()) {
			throw new UserNotFound(Constants.NO_USER_FOUND);
		}

		booking.setUser(optionalUser.get());
		booking.setBookingDate(LocalDate.now());
		booking.setBookingStatus(false);
		booking.setPaymentStatus(Constants.PENDING);

		List<PassengerDto> passengers = bookingRequest.getPassengers();
		if(passengers.size()>5) {
			throw new PassengerLimitExceededException(Constants.MAX_FIVE_PASSENGERS);
		}

		Long bookingNumber = Long.valueOf(new Random().nextInt(50000));

		booking.setBookingNumber(bookingNumber);

		Optional<TrainClass> optionalTrainClass = trainClassRepository.findById(bookingRequest.getTrainClassId());

		if (!optionalTrainClass.isPresent()) {
			throw new TrainClassNotFound(Constants.NO_TRAIN_CLASS_FOUND);
		}

		if (optionalTrainClass.get().getAvailableSeats() <= 0
				|| optionalTrainClass.get().getAvailableSeats() < passengers.size()) {
			throw new NoSeatAvailable(Constants.SEATS_ARE_NOT_AVAIALABLE);
		}

		Double amount = optionalTrainClass.get().getFare() * passengers.size();
		booking.setNoOfSeats(passengers.size());
		booking.setTotalAmount(amount);

		Optional<Train> optionalTrain = trainRepository.findById(bookingRequest.getTrainId());
		if (!optionalTrain.isPresent()) {
			throw new TrainNotFoundExceptionHandler(Constants.TRAIN_NOT_FOUND);
		}

		booking.setTrainName(optionalTrain.get().getTrainName());

		bookingRepository.save(booking);

		passengers.forEach(passengerInBooking -> {
			Passenger passenger = new Passenger();
			BeanUtils.copyProperties(passengerInBooking, passenger);
			passenger.setBook(booking);
			passengerRepository.save(passenger);
		});

		optionalTrainClass.get().setAvailableSeats(optionalTrainClass.get().getAvailableSeats() - passengers.size());
		
		logger.info("saving train class repo");
		
		trainClassRepository.save(optionalTrainClass.get());

		BookingResponse bookingResponse = new BookingResponse();
		bookingResponse.setBookingNumber(bookingNumber);
		bookingResponse.setAmount(amount);
		bookingResponse.setResponseCode(200L);
		bookingResponse.setResponseMessage(Constants.BOOKING_TICKET);

		return bookingResponse;
	}

	@Override
	public List<BookingHistory> getBookingHistory(Long userId) {
		Optional<User> findById = userRepository.findById(userId);
		User user=new User();
		if(findById.isPresent()) {
			user= findById.get();
		}
		List<Booking> booking = user.getBooking();
		List<BookingHistory> bookingList = new ArrayList<>();
		booking.forEach(temp -> {
			BookingHistory bookingHistory = new BookingHistory();
			bookingHistory.setBookingDate(temp.getBookingDate());
			bookingHistory.setBookingId(temp.getBookingId());
			bookingHistory.setBookingNumber(temp.getBookingNumber());
			bookingHistory.setBookingStatus(temp.getBookingStatus());
			bookingHistory.setNoOfSeats(temp.getNoOfSeats());
			bookingHistory.setTotalAmount(temp.getTotalAmount());
			bookingHistory.setTrainName(temp.getTrainName());
			bookingHistory.setPaymentStatus(temp.getPaymentStatus());
			bookingList.add(bookingHistory);
		});
		return bookingList;

	}

}

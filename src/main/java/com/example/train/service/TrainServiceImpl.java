/**
 * 
 */
package com.example.train.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.train.constants.Constants;
import com.example.train.entity.Train;
import com.example.train.entity.TrainClass;
import com.example.train.exception.TrainNotFoundExceptionHandler;
import com.example.train.repository.TrainRepository;
import com.example.train.response.TrainClassDto;
import com.example.train.response.TrainDetailResponse;
import com.example.train.response.TrainResponse;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public class TrainServiceImpl implements TrainService {

	/**
	 * 
	 */
	
	@Autowired

	TrainRepository trainrepository;
	
	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public List<TrainResponse> search(String source, String destination, LocalDate availableDate) {
		List<TrainResponse> response = new ArrayList<>();
		logger.info("TrainServiceImpl : search ");
		List<Train> trainsEntities = trainrepository
				.findBySourceIgnoreCaseContainingAndDestinationIgnoreCaseContainingAndTrainRunningDate(source,
						destination, availableDate);

		trainsEntities.forEach(p -> {
			TrainResponse dto = new TrainResponse();
			BeanUtils.copyProperties(p, dto);
			response.add(dto);
		});

		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.example.train.service.TrainService#search(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */

	@Override
	public TrainDetailResponse searchById(Long trainId) {

		Optional<Train> findById = trainrepository.findById(trainId);
		if (!findById.isPresent()) {
			throw new TrainNotFoundExceptionHandler(Constants.TRAIN_NOT_FOUND);
		}
		Train train = findById.get();
		List<TrainClass> trainClass = train.getTrainClass();
		TrainDetailResponse trainDetailResponse = new TrainDetailResponse();
		BeanUtils.copyProperties(train, trainDetailResponse);
		
		logger.info("TrainServiceImpl : searchById ");

		List<TrainClassDto> trainseatList = new ArrayList<>();
		trainClass.forEach(temp -> {
			TrainClassDto trainSeat = new TrainClassDto();
			trainSeat.setAvailableSeats(temp.getAvailableSeats());
			trainSeat.setTrainClassId(temp.getTrainClassId());
			trainSeat.setClassName(temp.getClassName());
			trainSeat.setFare(temp.getFare());

			trainseatList.add(trainSeat);
		});
		trainDetailResponse.setTrainClass(trainseatList);
		return trainDetailResponse;
	}

}

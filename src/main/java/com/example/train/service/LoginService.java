/**
 * 
 */
package com.example.train.service;

import org.springframework.stereotype.Service;

import com.example.train.exception.UserNotFound;
import com.example.train.request.LoginDto;
import com.example.train.response.ResponseDto;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public interface LoginService {

	public ResponseDto userLogin(LoginDto loginDto) throws UserNotFound;
}

/**
 * 
 */
package com.example.train.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;
import com.example.train.utils.Paymentservice;

import lombok.extern.slf4j.Slf4j;

/**
 * @author rakeshr.ra
 *
 */
@Service("googlepay")
@Slf4j
public class GooglePay implements AdapatorService<PaymentRequest> {

	/**
	 * 
	 */

	

	@Autowired
	Paymentservice paymentservice;

	@Override
	public ResponseDto process(PaymentRequest request) {

		return paymentservice.processPayment(request);

	}
}
package com.example.train.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.train.exception.UserNotFound;
import com.example.train.request.BookingRequest;
import com.example.train.response.BookingHistory;
import com.example.train.response.BookingResponse;

@Service
public interface UserService {
	public BookingResponse bookTicket(BookingRequest bookingRequest) throws UserNotFound;

	List<BookingHistory> getBookingHistory(Long userId);
}

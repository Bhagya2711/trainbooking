package com.example.train.service;
/**
 * @author rakeshr.ra
 *
 */
public interface ServiceRegistry {
	
	public <T> AdapatorService<T> getService(String serviceName);

}

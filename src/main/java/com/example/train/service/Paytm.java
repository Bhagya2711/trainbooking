package com.example.train.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;
import com.example.train.utils.Paymentservice;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author rakeshr.ra
 *
 */
@Service("paytm")
@Slf4j
public class Paytm implements AdapatorService<PaymentRequest> {


	/**
	 * 
	 */

	
	@Autowired
	Paymentservice paymentservice;

	@Override
	public ResponseDto process(PaymentRequest request) {
		log.info("into the paytm");
		return paymentservice.processPayment(request);

	}

}

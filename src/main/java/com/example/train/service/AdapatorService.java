package com.example.train.service;
/**
 * @author rakeshr.ra
 *
 */
import com.example.train.response.ResponseDto;

public interface AdapatorService<T> {

	public ResponseDto process(T request);

}

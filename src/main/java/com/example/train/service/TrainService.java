package com.example.train.service;

import java.time.LocalDate;
import com.example.train.response.TrainDetailResponse;

/**
 * 
 * @author rakeshr.ra
 *
 */
public interface TrainService {

	/**
	 * @param source
	 * @param destination
	 * @param availableDate
	 * @return
	 */
	Object search(String source, String destination, LocalDate availableDate);
	
	
	TrainDetailResponse searchById(Long trainId);


}

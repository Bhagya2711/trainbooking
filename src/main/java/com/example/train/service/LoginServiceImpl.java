package com.example.train.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.train.constants.Constants;
import com.example.train.entity.User;
import com.example.train.exception.UserNotFound;
import com.example.train.repository.UserRepository;
import com.example.train.request.LoginDto;
import com.example.train.response.ResponseDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {
	/**
	 * 
	 */
		@Autowired
	UserRepository userRepository;
	
	/**
	 * @param loginDto
	 * @return ResponseDto
	 */
	@Override
	public ResponseDto userLogin(LoginDto loginDto) throws UserNotFound {
		log.info("Login service ");
		Optional<User> optionalUser = userRepository.findByUserEmailAndUserPassword(loginDto.getUserEmail(),
				loginDto.getUserPassword());
		if (!optionalUser.isPresent()) {
			throw new UserNotFound(Constants.USER_NOT_FOUND);
		}
		return new ResponseDto(Constants.LOGGED_IN_SUCCESSFULLY, 200L);

	}

}

/**
 * 
 */
package com.example.train.enums;

/**
 * @author hemanth.garlapati
 *
 */
public enum ErrorCodes {
	USER_NOT_FOUND(4041l),VALIDATION_FAILED(4001l)
	,TRAIN_NOT_FOUND(4043l),TRAIN_CLASS_NOT_FOUND(4042l),SEAT_NOT_AVAILABLE(4049l)
	
	,BOOKING_ID_NOT_FOUND(4200l),PASSENGER_LIMIT_EXCEEDED(4046l);

	Long errorCode;

	ErrorCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}

/**
 * 
 */
package com.example.train.config;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.train.service.ServiceRegistry;

/**
 * @author rakeshr.ra
 *
 */
@Configuration
public class PaymentConfig {
	@Bean
	public FactoryBean<?> factoryBean() {

		final ServiceLocatorFactoryBean locator = new ServiceLocatorFactoryBean();
		locator.setServiceLocatorInterface(ServiceRegistry.class);
		return locator;
	}

}

/**
 * 
 */
package com.example.train.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author hemanth.garlapati
 * 
 */
@Component
@ConfigurationProperties
public class ConfigurationManager {

}

/**
 * 
 */
package com.example.train.repository;

import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import com.example.train.entity.Booking;

public interface BookingRepository extends JpaRepository<Booking, Long> {
	
	Optional<Booking> findByBookingNumber(Long bookingnumber);
	
	@Lock(LockModeType.PESSIMISTIC_READ)
	Booking save(Booking booking);

}

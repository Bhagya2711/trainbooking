/**
 * 
 */
package com.example.train.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.train.entity.Passenger;

/**
 * @author hemanth.garlapati
 * 
 */
@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long>{

}

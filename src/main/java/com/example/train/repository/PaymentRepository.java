/**
 * 
 */
package com.example.train.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.train.entity.Payment;

/**
 * @author rakeshr.ra
 *
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

}

package com.example.train.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.train.entity.Train;

/**
 * 
 * @author rakeshr.ra
 *
 */
@Repository
public interface TrainRepository extends JpaRepository<Train, Long> {

	/**
	 * @param source
	 * @param destination
	 * @param availableDate
	 */
	List<Train> findBySourceAndDestinationAndTrainRunningDate(String source, String destination, LocalDate availableDate);

	/**
	 * @param source
	 * @param destination
	 * @param availableDate
	 * @return
	 */
	List<Train> findBySourceIgnoreCaseContainingAndDestinationIgnoreCaseContainingAndTrainRunningDate(String source,
			String destination, LocalDate availableDate);

}

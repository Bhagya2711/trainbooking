/**
 * 
 */
package com.example.train.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.train.entity.User;

/**
 * @author hemanth.garlapati
 * 
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	Optional<User> findByUserEmailAndUserPassword(String userEmail,String userPassword);

}

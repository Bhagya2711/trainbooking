package com.example.train.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;
import com.example.train.service.ServiceRegistry;

/**
 * 
 * @author rakeshr.ra
 *
 */
@RestController
@RequestMapping("/payments")
public class PaymentController {
	Logger logger = LoggerFactory.getLogger(PaymentController.class);

	@Autowired
	ServiceRegistry serviceRegistry;

	/**
	 * @author rakeshr.ra
	 * @param paymentRequest
	 * @return success message with status code
	 */
	@PostMapping
	public ResponseEntity<ResponseDto> processPayment(@Valid @RequestBody PaymentRequest paymentRequest) {

		logger.info("PaymentController :_processPayment");
		return new ResponseEntity<>(serviceRegistry.getService(paymentRequest.getPaymentType()).process(paymentRequest),
				HttpStatus.OK);

	}

}

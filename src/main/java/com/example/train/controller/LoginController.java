/**
 * 
 */
package com.example.train.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.train.exception.UserNotFound;
import com.example.train.request.LoginDto;
import com.example.train.response.ResponseDto;
import com.example.train.service.LoginService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

/**
 * @author hemanth.garlapati
 * 
 */
@RestController
public class LoginController {

	@Autowired
	LoginService loginService;
	Logger logger = LoggerFactory.getLogger(LoginController.class);

	/**
	 * @param loginDto
	 * @return ResponseDto
	 */
	@ApiOperation(value = "User Login")
	@ApiResponse(code = 4041, message = "User not found")
	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@Valid @RequestBody LoginDto loginDto) throws UserNotFound {
		logger.info("LoginController: userLogin ");
		return new ResponseEntity<>(loginService.userLogin(loginDto), HttpStatus.OK);
	}

}

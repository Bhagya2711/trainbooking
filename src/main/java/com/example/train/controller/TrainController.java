package com.example.train.controller;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.train.response.TrainDetailResponse;
import com.example.train.service.TrainService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/trains")
public class TrainController {

	Logger logger = LoggerFactory.getLogger(TrainController.class);

	@Autowired
	TrainService trainService;

	/**
	 * @param source
	 * @param destination
	 * @param availability
	 * @return
	 */
	@ApiOperation(value = "search a train")
	@GetMapping()
	public ResponseEntity<Object> search(@RequestParam String source, @RequestParam String destination,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate availableDate

	) {
		logger.info("TrainController: Inside search method");
		return new ResponseEntity<>(trainService.search(source, destination, availableDate), HttpStatus.OK);
	}

	@ApiOperation(value = "search a train by trainid")
	@GetMapping("/{trainId}")
	public ResponseEntity<TrainDetailResponse> searchById(@PathVariable Long trainId) {
		logger.info("TrainController: Inside search by Train id method");
		return new ResponseEntity<>(trainService.searchById(trainId), HttpStatus.OK);

	}

}

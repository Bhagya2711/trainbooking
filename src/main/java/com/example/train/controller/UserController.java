package com.example.train.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.train.exception.UserNotFound;
import com.example.train.request.BookingRequest;
import com.example.train.response.BookingHistory;
import com.example.train.response.BookingResponse;
import com.example.train.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author rakeshr.ra
 *
 */
@RestController


public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	UserService userService;

	@GetMapping("/users/{userId}/tickets")
	public ResponseEntity<List<BookingHistory>> getBookingHistory(@PathVariable Long userId) {
		logger.info("UserController : getBookingHistory");
		return new ResponseEntity<>(userService.getBookingHistory(userId), HttpStatus.OK);

	}

	/**
	 * This method is for booking ticket
	 * 
	 * @param bookingRequest
	 * @return BookingResponse
	 */
	@ApiOperation(value = "Booking Ticket")
	@ApiResponses(value = { @ApiResponse(code = 4043, message = "No train Found"),
			@ApiResponse(code = 4042, message = "No train class found"),
			@ApiResponse(code = 4046, message = "max of 5 passengers are allowed for booking"),
			@ApiResponse(code = 4049, message = "Seats are not available") })
	@PostMapping("/user-tickets")
	public ResponseEntity<BookingResponse> bookTicket(@RequestBody BookingRequest bookingRequest) throws UserNotFound {
		logger.info("UserController : bookTicket");
		return new ResponseEntity<>(userService.bookTicket(bookingRequest), HttpStatus.OK);
	}

}

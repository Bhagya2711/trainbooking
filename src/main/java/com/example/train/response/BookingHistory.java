package com.example.train.response;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingHistory {

	private Long bookingId;
	private Long bookingNumber;
	private LocalDate bookingDate;
	private Boolean bookingStatus;
	private Double totalAmount;
	private String trainName;
	private Integer noOfSeats;
	private String paymentStatus;
}

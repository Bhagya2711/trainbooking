package com.example.train.response;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrainDetailResponse {
	private Long trainId;
	private Long trainNumber;
	private String trainName;
	private String source;
	private String destination;
	private LocalTime departureTime;
	private LocalTime arrivalTime;
	private LocalDate trainRunningDate;

	
	List<TrainClassDto> trainClass;
}

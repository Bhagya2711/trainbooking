package com.example.train.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrainClassDto {
	private Long trainClassId;

	private String className;

	private Integer availableSeats;
	private Double fare;
}

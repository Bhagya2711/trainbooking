/**
 * 
 */
package com.example.train.response;

import java.time.LocalTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrainResponse {

	private Long trainId;
	private String trainName;
	private Long trainNumber;
	private String source;
	private String destination;
	private LocalTime departureTime;
	private LocalTime arrivalTime;

}

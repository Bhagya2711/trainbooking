package com.example.train.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author hemanth.garlapati
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingResponse {

	private String responseMessage;
	private Long bookingNumber;
	private Double amount;
	private Long responseCode;

}

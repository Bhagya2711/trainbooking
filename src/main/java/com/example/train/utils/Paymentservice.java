package com.example.train.utils;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.train.constants.Constants;
import com.example.train.entity.Booking;
import com.example.train.entity.Payment;
import com.example.train.exception.BookinIdException;
import com.example.train.repository.BookingRepository;
import com.example.train.repository.PaymentRepository;
import com.example.train.request.PaymentRequest;
import com.example.train.response.ResponseDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Paymentservice {

	@Autowired
	PaymentRepository paymentRepository;
	@Autowired
	BookingRepository bookingRepository;

	@Transactional
	public ResponseDto processPayment(PaymentRequest request) {
		log.info("in service{}", request.getPaymentType());
		Optional<Booking> findByBookingNumber = bookingRepository.findByBookingNumber(request.getBookingNumber());
		
		if (!findByBookingNumber.isPresent()) {
			throw new BookinIdException(Constants.BOOKING_ID_NOT_FOUND);
		}
		Booking booking = findByBookingNumber.get();
		if (booking.getBookingStatus()) {
			log.info("paytm :payment has made earlier");
			throw new BookinIdException(Constants.PAYMENT_HAS_MADE_EARLIER);
		}
		
		 if (booking.getTotalAmount() == request.getAmount()) {


	            booking.setPaymentStatus("success through " + request.getPaymentType());
	            booking.setBookingStatus(true);
	            bookingRepository.save(booking);
	            Payment payment = new Payment();
	            payment.setAmount(request.getAmount());
	            payment.setBookingNumber(request.getBookingNumber());
	            payment.setPaymentDate(LocalDate.now());
	            payment.setPaymentMethod(request.getPaymentType());
	            payment.setPaymentStatus("success");
	            paymentRepository.save(payment);


	            return new ResponseDto(Constants.PAYMENT_SUCCESS, 200L);
	        }


	        else {
	            throw new BookinIdException("please provide valid amount");
	        }

		

	}
}

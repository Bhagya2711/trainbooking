package com.example.train.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LoginDto {

	@NotNull(message ="User mail is mandatory")
	@Email
	private String userEmail;

	
	@NotNull(message ="Password is mandatory")
	private String userPassword;
	
}

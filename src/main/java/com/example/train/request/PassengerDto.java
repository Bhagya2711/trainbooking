/**
 * 
 */
package com.example.train.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rakeshr.ra
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassengerDto {

	private String passengerName;
	private Integer passengerAge;
	private String aadharNumber;

}

package com.example.train.request;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rakeshr.ra
 *
 */
@Getter
@Setter
@Component
public class PaymentRequest {

	@NotNull(message ="Payment type is mandatory")
	String paymentType;
	@NotNull(message ="Amount  is mandatory")
	double amount;
	@NotNull(message ="Booking no. is mandatory")
	Long bookingNumber;

}

package com.example.train.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author hemanth.garlapati
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingRequest {

	private Long trainId;
	private Long trainClassId;
	private Long userId;
	List<PassengerDto> passengers;

}

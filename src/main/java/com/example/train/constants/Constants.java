/**
 * 
 */
package com.example.train.constants;

/**
 * @author hemanth.garlapati
 * 
 */
public class Constants {
	
	private Constants(){
		
	}
	
	public static final String USER_NOT_FOUND = "User not found";
	public static final String PENDING = "pending";

	public static final String LOGGED_IN_SUCCESSFULLY = "Logged in successfully";

	public static final String TRAIN_NOT_FOUND = "train not found";
	
	
	public static final String BOOKING_TICKET = "Booking Ticket";

	public static final String NO_TRAIN_CLASS_FOUND = "No train class found";

	
	public static final String NO_USER_FOUND = "No user Found";
	
	public static final String SEATS_ARE_NOT_AVAIALABLE = "Seats are not avaialable";

	public static final String PAYMENT_SUCCESS = "payment success";
	public static final String SUCCESS = "success";
	public static final String SUCCESS_THROUGH_GOOGLEPAY = "success through googlepay";
	public static final String BOOKING_ID_NOT_FOUND = "booking id not found";

	public static final String SUCCESS_THROUGH_PAYTM = "Success through paytm";
	public static final String SUCCESS_THROUGH_PHONEPE = "Success through phonepe";
	public static final String PAYMENT_HAS_MADE_EARLIER = "payment has made earlier";
	public static final String MAX_FIVE_PASSENGERS="max of five passengers are allowed for booking at once"; 
}

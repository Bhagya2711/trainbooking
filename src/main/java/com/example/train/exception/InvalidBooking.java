package com.example.train.exception;

public class InvalidBooking extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidBooking(String message) {
		super(message);

	}

}

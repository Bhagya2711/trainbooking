/**
 * 
 */
package com.example.train.exception;

/**
 * @author hemanth.garlapati
 * 
 */
public class TrainClassNotFound extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainClassNotFound(String arg0) {
		super(arg0);
		
	}
}

package com.example.train.exception;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.train.enums.ErrorCodes;

/**
 * 
 * @author rakeshr.ra
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse error = new ErrorResponse(ErrorCodes.VALIDATION_FAILED.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ValidationException.class)
	public final ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ErrorCodes.VALIDATION_FAILED.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserNotFound.class)
	public final ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFound ex) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ErrorCodes.USER_NOT_FOUND.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(TrainNotFoundExceptionHandler.class)
	public ResponseEntity<ErrorResponse> handleTrainNotFound(TrainNotFoundExceptionHandler details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse er = new ErrorResponse(ErrorCodes.TRAIN_NOT_FOUND.getErrorCode(), listError);

		return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(TrainClassNotFound.class)
	public ResponseEntity<ErrorResponse> handleTrainClassNotFound(TrainClassNotFound details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse er = new ErrorResponse(ErrorCodes.TRAIN_CLASS_NOT_FOUND.getErrorCode(), listError);

		return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(NoSeatAvailable.class)
	public ResponseEntity<ErrorResponse> handleNoSeatAvailable(NoSeatAvailable details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse er = new ErrorResponse(ErrorCodes.SEAT_NOT_AVAILABLE.getErrorCode(), listError);

		return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(BookinIdException.class)
	public ResponseEntity<ErrorResponse> bookingIDNotAvailable(BookinIdException details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse er = new ErrorResponse(ErrorCodes.BOOKING_ID_NOT_FOUND.getErrorCode(), listError);

		return new ResponseEntity<>(er, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(PassengerLimitExceededException.class)
	public ResponseEntity<ErrorResponse> handlePassengerLimitExceededException(PassengerLimitExceededException details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse er = new ErrorResponse(ErrorCodes.PASSENGER_LIMIT_EXCEEDED.getErrorCode(), listError);

		return new ResponseEntity<>(er, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);

	}
	

}

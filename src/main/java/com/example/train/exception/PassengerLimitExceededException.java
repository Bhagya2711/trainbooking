package com.example.train.exception;

public class PassengerLimitExceededException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PassengerLimitExceededException(String message) {
		super(message);
	
	}
	

}

/**
 * 
 */
package com.example.train.exception;

/**
 * @author hemanth.garlapati
 * 
 */
public class NoSeatAvailable extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoSeatAvailable(String arg0) {
		super(arg0);
		
	}

}

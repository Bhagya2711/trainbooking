package com.example.train.exception;

public class BookinIdException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookinIdException(String message) {
		super(message);
	
	}
	

}

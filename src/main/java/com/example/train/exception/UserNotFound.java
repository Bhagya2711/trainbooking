/**
 * 
 */
package com.example.train.exception;

/**
 * @author hemanth.garlapati
 * 
 */
public class UserNotFound extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public UserNotFound(String message) {
		super(message);
	}

}

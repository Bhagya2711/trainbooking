/**
 * 
 */
package com.example.train.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rakeshr.ra
 *
 */
@Entity
@Getter
@Setter
public class Booking {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bookingId;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	@OneToMany(mappedBy = "book")
	private List<Passenger> passengers;
	private Long bookingNumber;
	private LocalDate bookingDate;
	private String paymentStatus;
	private Double totalAmount;
	private String trainName;
	private Integer noOfSeats;
	private Boolean bookingStatus;
}

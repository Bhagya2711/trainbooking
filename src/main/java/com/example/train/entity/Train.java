package com.example.train.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author hemanth.garlapati
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Train {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long trainId;
	private Long trainNumber;
	private String trainName;
	private String source;
	private String destination;
	private LocalTime departureTime;
	private LocalTime arrivalTime;
	private LocalDate trainRunningDate;

	@OneToMany(mappedBy = "train")
	List<TrainClass> trainClass;

}

package com.example.train.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author rakeshr.ra
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	private String userName;
	@Column(unique = true)
	private String userEmail;
	private String userPassword;
	@OneToMany(mappedBy = "user")

	List<Booking> booking;

}

/**
 * 
 */
package com.example.train.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TrainClass {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long trainClassId;

	private String className;

	private Integer availableSeats;

	
	@ManyToOne
	@JoinColumn
	Train train;

	private Double fare;

}
